package basics;

import java.util.Locale;
import java.util.Scanner;

public class ControlOfFlow {


    // Control of Flow
    // 1) if statement

    // Syntax : if (<boolean_expression>)  statement; or {block} [else statement; or {block}]

    // 2) switch statement

    // syntax : switch(<switch_expression>)
    // {
    //    [case <unique_constant_value> : [statement]*]*
    //    [default: [statement] *]
    // }

    //  switch_expression : value | variable (boolean, char, byte,short, int , string and enum)

    // 3) if-else-then (conditional operator) (ternary operator)
    // syntax : <boolean_expression> ? <value> : <value>


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.getDefault());
        final int c = 1;
        int a = 5;



        if (a == 5)
            System.out.println("Yes, it is.");
        else {
            System.out.println("No");

            System.out.println("Done");
        }


        final  int d = 5 ;
        switch (d)
        {
            case  5 :
                System.out.println("");
            case  8:
                System.out.println();

        }
        a = 5 ;
        switch (a) {

            case 0:
            case d-2:
                System.out.println("One");
                break;
            case 2:
                System.out.println("zero");
                break;

            default:
                System.out.println("default");
                break;


        }
        /*
                35 - <50     -> Failed
                50 - <68     -> Passed
                68 - <76     -> Good
                76 - <84     -> Very Good
                84 - 100     -> Excellent
                other        -> Error
         */

        boolean test = false;
        if (test = true)
            System.out.println();

        int x = 5;

        if (x == 5)
            System.out.println("Yes");
        else {
            System.out.println("No");
            System.out.println("Done");
        }


        System.out.println(a > 0 ? 10: 20);
        int r = a > 0 ? 10: 20 ;

        if(a > 0 )
        {
            System.out.println();
            r = 10 ;}
        else
            r = 20 ;


        //a = 2 ;
        if (a == 2)
            System.out.println("Yes");
        else
            System.out.println("No");

        System.out.println("Done");

        int abs;
        if (a > 0)
            System.out.println("abs = " + (abs = a));
        else
            System.out.println("abs = " + (abs = -a));

        System.out.println("abs = " + (abs = a > 0 ? a : -a));

        // System.out.println("abs = " + abs);

        System.out.print("mark:");
        double mark = scanner.nextDouble();

        if (mark < 35 || mark > 100)
            System.out.println("Error");
            // 35 <= mark <= 100
        else if (mark < 50)
            System.out.println("Failed");
            // 50 <= mark <= 100
        else if (mark < 68)
            System.out.println("Passed");
            // 68 <= mark <= 100
        else if (mark < 76)
            System.out.println("Good");
            // 76 <= mark <= 100
        else if (mark < 84)
            System.out.println("Very good");
            // 84 <= mark <= 100
        else
            System.out.println("Excellent");


        System.out.println(mark < 35 || mark > 100 ? "Error" : mark < 50 ? "Failed" : mark < 68 ? "Passed" : mark < 76 ? "Good" : mark < 84 ? "Very good" : "Excellent");


        scanner.close();

    }
}

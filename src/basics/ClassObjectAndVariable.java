package basics;

public class ClassObjectAndVariable {

    // class : is description/blueprint/template for a data type that describe its stats (variables and objects) and behaviours (methods)

    // object : instance of a class.

    // variable : named reference to a memory location

    // class variable


    static class  Employee {
        static  int _id = 0 ;

        int ID ;
    }

    static class  Player {
        String name ;
        int age ;
        static  int NumberOfLegs = 2 ;

    }
    static class  Desh {
        int some ;


    }

    public static void main(String[] args) {


        Hello h = new Hello();

        Player p  = new Player();
        p.age = 10 ;
        Player p1 = new Player();

        p1.age = 50 ;

        System.out.println(p.age);
        System.out.println(p1.age);

        System.out.println("Player.NumberOfLegs = " + Player.NumberOfLegs);

        Player.NumberOfLegs = 5 ;
        System.out.println("Player.NumberOfLegs = " + Player.NumberOfLegs);



        new Player();
//        Desh desh = new Desh();
    }
}

package basics;

public class Hello {

    public static void main(String[] args) {
        System.out.println("Welcome to Java");

        System.out.println(10 + 3 + 5);        // 18
        System.out.println("10" + 3 + 5);      // 1035
        System.out.println("10" + (3 + 5));    // 108
        System.out.println(10 + "3" + 5);      // 1035
        System.out.println(10 + 3 + "5");      // 135
        System.out.println(10 + '3' + 5);      // 66
        System.out.println(10 + '3' + '5');    // 114
        System.out.println(10 + 'A' + 5);      // 80

    }
}





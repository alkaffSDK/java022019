package basics;

import java.util.Locale;

public class StringFormating {
    public static void main(String[] args) {

        main(args);

        double gold = 32.15 ;
        double silver = 2.78 ;
        double diamond = 1425.5 ;

        System.out.println("Gold    :"+ gold);
        System.out.println("Silver  :"+ silver);
        System.out.println("Diamond :"+ diamond);


        System.out.printf(Locale.US,"%-10s:%10.4f%n","Gold",gold);
        System.out.printf(Locale.US,"%-10s:%10.4f%n","Silver",silver);
        System.out.printf(Locale.US,"%-10s:%10.4f%n","Diamond",diamond);
    }
}

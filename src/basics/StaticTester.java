package basics;

public class StaticTester {
    public static void main(String[] args) {

        StaticDemo demo = new StaticDemo();
        demo.instance_var = 10 ;
        demo.method();

        StaticDemo.static_variable = 20 ;
        StaticDemo.StaticMethod();

        demo.method();

        StaticDemo.StaticMethod();
        StaticDemo.main(args);
    }
}

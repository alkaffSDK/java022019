package basics;

public  class StaticDemo {

    class Inner {

    }
    static class StaticInner {

    }

    int instance_var ;
    static int static_variable ;        /// class variables

    public static void main(String[] args) {
            int local ;         /// can't use static modifier with local variable
    }

    public void method()
    {
        instance_var = 20 ;
        static_variable = 10 ;
    }

    static public void StaticMethod()
    {
        // instance_var = 20 ; // can't use instance variables from static methods
        StaticDemo obj = new StaticDemo();
        obj.instance_var = 10 ;
        static_variable  = 10 ;
    }

}

package basics;

import java.io.*;
import java.util.Locale;
import java.util.Scanner;


public class ConsoleInput {
    public static void main(String[] args) {

      
        // create scanner object
        Scanner in = new Scanner(System.in);
        // use the default localization settings
        in.useLocale(Locale.getDefault());

        // use the scanner object to input from console

        System.out.print("Please enter a value for A:");
        int a = in.nextInt() ;
        System.out.println("a = " + a);


        System.out.print("Please enter a value for D:");
        double d = in.nextDouble() ;
        System.out.println("d = " + d);
        // To remove the next line after the double
        in.nextLine();

        System.out.print("Please enter a full line:");
        String input = in.nextLine();
        System.out.println("input = " + input);

        System.out.print("Please enter a full line:");
       input = in.next();
        System.out.println("input1 = " + input);

        input = in.next();
        System.out.println("input2 = " + input);

        input = in.next();
        System.out.println("input3 = " + input);

        // don't forget to close the scanner !
        in.close();

    }
}

package basics;

import java.math.BigInteger;
import java.util.ArrayList;

public class Variables {

    int instanceVariable ;                 // instance variable

    static int classVariable ;              // class variable

    public static void main(String[] args) {


        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("s");
        double k ;
        k =2;
        // Variable : is a symbolic name (an identifier) paired with a storage location (identified by a memory address)
        // which contains some known or unknown quantity of information referred to as a value.

        // In Java variables must be defined before been used.

        // Example :
        int x = 10;                     // x is a variable
        Object o = new Object();        // o is a variable


        // Syntax: to define a variable
         //        [modifiers] [specifier] [specifier] <data_type> <variable_identifier> [=<expression>][, <variable_identifier> [=<expression>]]*;

    // <data_type> <variable_identifier> [=<expression>] ;

        int a = Math.abs( 15 + 2);

        int m , n , r  = 15;
        x = 20 ;

        // Zalid variable identifier
        int c$d , c_d  ;

        // Invalid variable identifier
        //  c-d , 3d , int

        System.out.println(33);

        // Type of variables based it's location

        // variable can be defined in one of three places (locations)

        // 1) inside a block (like a method)            (local variable)
        // 2) inside an object                          (instance variable)
        // 3) inside a class                            (static / class variable)

        // ** Note : both 2 and 3 are *written* inside the class but the class variables are static


        int localVariable ;


        // Data types
        // 1) Primitive Data Types (value types) : boolean ,char ,byte, short, int ,long  ,float , double
        // 2) Reference/Object Data Types  : any other types


        boolean b = true ;


        char ch = 'A';
        System.out.println("ch = ." + ch);
        ch = 65;                // decimal Unicode code
        System.out.println("ch = ." + ch);
//        ch = 101;
//        System.out.println("ch = ." + ch);
        ch = 0101;                  // In Java, C++ any number start with 0 from the left is an octal
        System.out.println("ch = ." + ch);
        ch = 0x41;                  // In Java, C++ any number start with 0x from the left is an Hexadecimal
        System.out.println("ch = ." + ch);

        ch = '\u0041';              // Hexadecimal Unicode code
        System.out.println("ch = ." + ch);


        char bs = '\u0008';

        System.out.println("Bs:"+bs);

        a = 10 ;
        System.out.println("A: "+ a);       // 10

        a = 010 ;
        System.out.println("A: "+ a);       // 8

        a = 0x10 ;
        System.out.println("A: "+ a);       // 16

        a = '\u0010' ;
        System.out.println("A: "+ a);       // 16



        byte b1 = 127 ;
        BigInteger big = new BigInteger("156546554855666545455554545555478785632588565255858754542347346454545454225454154555");

        System.out.println(big);


        long l = 9223372036854775807L ;// 9223372036854775807 ;

        float f = 10.54F ;          // F
        double d = 10.54 ;


        int val =  0x1abcdef ;

        // Type conventions :
        //  we can convert byte --> short--> int--> long --> float -->double


        int dd = (int) 15.2;

        int value = 2147483646 ;
        float target = value ;

        // there are some risky cases :
        //      1) int--> float : for big integer value

        System.out.println("value  = " + value);
        System.out.println("target = " + target);
        value = (int) target;
        System.out.println("value = " + value);
        System.out.println("----------------------------");

        //      2) long --> float : for big long value
        long longValue = Long.MAX_VALUE-10 ;
        target = longValue ;

        System.out.println("longValue  = " + longValue);
        System.out.println("target     = " + target);
        longValue = (long) target;
        System.out.println("longValue = " + longValue);
        System.out.println("----------------------------");

        //3) long --> double : for big long value

        double  dubTraget = longValue - 8 ;

        System.out.println("longValue  = " + longValue);
        System.out.println("dubTraget     = " + dubTraget);
        value = (int) target;
        System.out.println("value = " + value);
        System.out.println("----------------------------");


       // longValue = 10 ;
        int s = (int) longValue ;        // type casting


        int binary = 0410;
        int binary1 = 0x108;

        System.out.println("binary  = " + binary + " :"+ Integer.toBinaryString(binary)+":"+Integer.toHexString(binary));
        System.out.println("binary1 = " + binary1 + " :"+ Integer.toBinaryString(binary1)+":"+Integer.toHexString(binary1));

        System.out.println("Binary  :"+ Integer.toBinaryString(binary));
        System.out.println("Octal   :"+ Integer.toOctalString(binary));
        System.out.println("Decimal :"+ binary);
        System.out.println("Hexa    :"+ Integer.toHexString(binary));

        System.out.println("s = " + s);
        System.out.println("Integer.toBinaryString(s) = " + Integer.toBinaryString(s));

        // Cast from string to integer
        s = Integer.valueOf("1254");
        System.out.println("s = " + s);

        s = Integer.valueOf("1254", 8);
        System.out.println("s = " + s);

        s = Integer.valueOf("1001", 2);
        System.out.println("s = " + s);

        s = Integer.valueOf("41A", 16);
        System.out.println("s = " + s);


    }
    //

}

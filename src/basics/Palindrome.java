package basics;

import java.util.Locale;
import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.getDefault());
        String str = "", input, lower;
        int mid, last ;
        boolean isPalindrome = true ;
        do {
            System.out.print("Enter your string:");
            str = scanner.nextLine().trim() ;
            if(str == null || str.isBlank() || str.isEmpty())
                System.out.println("The string is empty");
            else if(str.length() == 1)
                System.out.printf("The string \"%s\" is palindrome.%n",str);
            else {
                isPalindrome = true ;
                lower = str.toLowerCase() ;
                mid = lower.length() / 2 ;
                last = lower.length() -1 ;
                for(int i=0;i<mid;i++, last--)
                {
                    if(lower.charAt(i) != lower.charAt(last))
                    {
                        isPalindrome = false ;
                        break;
                    }
                }
                System.out.printf("The string \"%s\" is %s palindrome.%n",str, isPalindrome? "":"not");
            }

            System.out.println("Do you want to test new string (Yes/No)");
            input = scanner.nextLine().trim();
        }while (input.toLowerCase().startsWith("y")) ;


//        StringBuilder stringBuilder = new StringBuilder(str);
//        if(stringBuilder.reverse().toString().equalsIgnoreCase(str))
//
        scanner.close();
    }
}

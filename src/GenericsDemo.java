import java.util.Random;

public class GenericsDemo {

    static  class Gen<A>{
        public A a ;
    }

    static class  Pair {

        // Key

        private Object Key ;

        // Value

        private Object Value ;

        public Object getKey() {
            return Key;
        }

        public void setKey(Object key) {
            Key = key;
        }

        public Object getValue() {
            return Value;
        }

        public void setValue(Object value) {
            Value = value;
        }
    }

    static class PairG<K,V>{

        private  K key;
        private  V value ;

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }
    public static void main(String[] args) {



        Gen<Integer> a = new Gen<>();

        Gen<String> b = new Gen<>() ;

        Gen<Integer> c = new Gen<>();



        Pair p1 = new Pair();
        p1.setKey(1);
        p1.setValue("String");


        Pair p2 = new Pair();
        p2.setKey("A");
        p2.setValue(new Random());


        method(p1);
        method(p2);


        // -------------

        PairG<Integer,String> p3 = new PairG<>();
        p3.setKey(10);
        p3.setValue("String");


        PairG<String,Random> p4 = new PairG<>();
        p4.setKey("5");
        p4.setValue(new Random());
        p4.getKey().charAt(0);
        p4.getValue().nextInt(10);

    }

    public static void method(Pair p)
    {
        equals(new Random(),10);

    }

    interface  Addable<T> {
        T add(T a , T b);
    }

    public static boolean equals(int a , int b ) {return  a == b ;}
    public static boolean equals(double a , double b ) {return  a == b ;}
    public static boolean equals(String a , String b ) {return  a .equals(b) ;}
    public static boolean equals(Object a , Object b ) {return  a .equals(b) ;}

    public static <T extends Addable >  T add(T a, T b)
    {
        return (T) a.add(a,b);
    }


    public  static <T extends  Comparable<T>>  int Compare(T a , T b)
    {
        return  a.compareTo( b) ;
    }
    public static <T extends  String,B> boolean equals(T a , B b ) {return  a .equals(b) ;}
}

import java.util.Random;

public class TemperatureFiled {

    private static final int GAP = 2 ;
    public static void main(String[] args) {

        Random r = new Random();
        int rows = 10, col = 20;
        double temp = 0.0, privous;
        double field[][] = new double[rows][col];

        field[0][0] = 20 + r.nextInt(15) + r.nextDouble();
        privous = field[0][0];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {

                while (field[i][j] == 0) {
                    temp = privous + r.nextInt(GAP+1) + r.nextDouble() - 1;
                    //System.out.printf("[%d,%d]:%6.2f\n",i,j,temp);
                    if (j > 0 && GAP < Math.abs(field[i][j-1] - temp))
                        continue;

                    if (j > 0 && i > 0
                            && GAP < Math.abs(field[i - 1][j - 1] - temp))
                        continue;

                    if (i > 0 && GAP < Math.abs(field[i - 1][j] - temp))
                        continue;

                    if (i > 0 && j < field[i].length - 1
                            && GAP < Math.abs(field[i - 1][j + 1] - temp))
                        continue;

                    field[i][j] = temp;
                    if(j == field[i].length - 1)
                        privous = field[i][j];
                    else
                        privous = field[i][0];
                    break;
                }
            }
        }

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.printf("%4.0f, ", field[i][j]);
            }
            System.out.println();
        }

    }

}

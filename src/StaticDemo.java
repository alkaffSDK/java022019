import  static java.lang.System.out ;
public class StaticDemo {
    // static can be used with
    // 1) Inner classes (any class inside another class)
    // 2) instance variables
    // 3) methods
    // 4) blocks
    // 5) import

    {
      // this is a block
    }

    static {
        // this is a static block
    }
     int instance ;


    // this class will be exist inside each object


    public static void main(String[] args) {

        //Error static int local ;  // static is not allowed to be used inside the the blocks (like methods)

        out.println();          // this possible because of  import  static java.lang.System.out ;


    }
}

public class Outer {
    public class NonStaticInner {

    }

    // this class will be exist inside the outer class
    public static class StaticInner {

    }
}

package oop;

public class ObjectClassDemo {


   static class  A {
        private int x , y ;


       @Override
       public boolean equals(Object a) {
           if(a instanceof  A)
           {
               A t = (A) a;
               return  x == t.x && y == t.y;
           }
           return  false ;

       }


       @Override
       protected A clone() throws CloneNotSupportedException {
           A a = new A();
           a.x = x ;
           a.y = y ;
           return a;
       }

       @Override
       public String toString() {
           return "A{" +
                   "x=" + x +
                   ", y=" + y +
                   '}';
       }
   }

    public static void main(String[] args) throws CloneNotSupportedException {

       A a = new A();

       a.x = 10;
       a.y = 20 ;

       A a2 = a.clone() ;

        System.out.println(a);
        System.out.println(a.toString());

        A a1 = new A();

        a1.x = 10;
        a1.y = 20 ;

        System.out.println(a == a1);        //
        System.out.println("a = " + a);
        System.out.println("a1 = " + a1);
        System.out.println("a2 = " + a2);
        System.out.println(a.equals(a1));
        int x  = 10;

        System.out.println(a.equals(x));

    }
}

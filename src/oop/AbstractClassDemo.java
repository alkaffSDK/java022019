package oop;

import java.util.Random;

public class AbstractClassDemo {


    // abstract method : is a method with no body and must be implemented by the concrete child classes
    // abstract class : is a class the may have an abstract methods
    // you can't create an object from an abstract class directly using new operator

    static abstract  class Shape {

        public Shape() {
            System.out.println("An object of Shape is created!");
        }

        @Override
        public String toString() {
            return "Shape{}";
        }

        public abstract double area();
    }

    static  class Triangle extends  Shape {

        public Triangle() {
        }

        public Triangle(double height, double base) {
           setHeight(height);
            setBase(base);
        }

        private double height, base ;

        public double getHeight() {
            return height;
        }

        public void setHeight(double height) {
            if(height > 0 )
                this.height = height;
        }

        public double getBase() {
            return base;
        }

        public void setBase(double b) {
            if(b > 0)
             base = b;
        }

        @Override
        public double area() {
            return (height* base)/ 2 ;
        }

        @Override
        public String toString() {
            return "Triangle{" +
                    "height=" + height +
                    ", base=" + base +
                    '}';
        }
    }

    static class Rectangle extends  Shape {
        private double height, width ;


        public Rectangle() {
            this(0,0);              // call the constructor in the same class
        }

        public Rectangle(double height, double width) {
           setHeight(height);
           setWidth(width);

        }

        public void setHeight(double h)
        {
            if(h >0)
                height =h ;
        }

        public double getHeight() {return  height ;}

        public double getWidth() {
            return width;
        }

        public void setWidth(double w) {
            if(w >0)
                width =w ;
        }

        @Override
        public double area() {
            return (height* width) ;
        }
        @Override
        public String toString() {
            return "Rectangle{" +
                    "height=" + height +
                    ", width=" + width +
                    '}';
        }
    }

    public static void main(String[] args) {

        Shape[] shapes = new Shape[10];
        Random random = new Random();

        Rectangle rectangle ;
        for(int i=0;i<shapes.length;i++)
        {
            switch (random.nextInt(2))
            {
                case 0:
                    // using the empty default constructor
                    rectangle = new Rectangle();
                    rectangle.setWidth(random.nextInt(10));
                    rectangle.setHeight(random.nextInt(10));
                    shapes[i] = rectangle ;
                    break;
                case 1:
                    // using the constructor with the parameters
                    shapes[i] = new Triangle(random.nextInt(10), random.nextInt(10)) ;
                    break;
//                case 2:
//                    shapes[i] = new Shape();break;
            }

            System.out.println(shapes[i]+ ", Area:"+shapes[i].area());
        }


        System.out.println("---------------------");
//        Shape s = new Shape();
//        s.area();

        // To create an object of an abstract class you have 2 options

        // 1) by creating an object of any concrete (non abstract) child class

        Shape s  = new Triangle();
            s.area() ; // this will call the area implementation of the Triangle class



        // 2) by using the anonymous inner type

        Shape s1 = new Shape() {
            @Override
            public double area() {
                System.out.println("Anonymous inner type:area");
                return 0;
            }
        };

        s1.area();              // Anonymous inner type:area


    }
}

package oop;

public class PolymorphismDemo {
    // Polymorphism types

    //  1) static Polymorphism (not important in Java)

    //          a) Method overloading : defining more than one method suing the the same name but different parameters
    public static int add(int a) {
        return a;
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static double add(double a) {
        return a;
    }
    //          b) Operator overloading (not exist in Java)


    // 2) Dynamic Polymorphism

    //          a) Parent referencing
    static class Human {
        public void print() {
            System.out.println("Human");
        }
    }

    static class Student extends Human {
        public int StudentID;

        public void print() {
            System.out.println("Student");
        }
    }

    static class Employee extends Human {
        public int EmployeeID;

        public void print() {
            System.out.println("Employee");
        }
    }

    //          b) Late binding


    public static void main(String[] args) {

        // Method overloading
        int m = 10, n = 20;
        double d = 15.32;
        add(d);


        // Parent referencing
//
        // super type variable can refers to any sub type object
        Human h = new Human();
        Human hs = new Student();
        Human he = new Employee();


        //  Late binding : calling the the same method using the same variable but getting different result
        h.print();         // Human
        hs.print();        // Student
        he.print();        // Employee


        Student s = new Student();
        //Student sh  = new Human();        // subclass variable  can NOT refers to super class object
        // sh.StudentID = 10 ;

        Employee e = new Employee();
        // Employee eh = new Human();    // subclass variable  can NOT refers to super class object

    }
}

package oop;

import basics.Hello;

import java.util.Locale;

public class ClassObjectAndVariable {

    // class : is description/blueprint/template for a data type that describe its stats
    // (variables and objects) and behaviours (methods)

    // object : instance of a class.

    // variable : named reference to a memory location

    // class variable


    static class  Employee {
        static  int _id = 0 ;

        int ID ;
    }

    static class  Player {
        String name ;
        int age ;
        static  int NumberOfLegs = 2 ;

    }
    static class  Desh {
        int some ;


    }
    static Player p3 ;
    public static void main(String[] args) {

        Hello h = new Hello();

        Player p  = new Player();
        System.out.println("p = " + p);
        p.name = "Player 1"; 
        p.age = 10 ;
        System.out.println("p.name = " + p.name);
        System.out.println("p.age = " + p.age);


        Player p1 = new Player();
        p1.name = "Player 2";
        p1.age = 50 ;

        System.out.println("p1.name = " + p1.name);
        System.out.println("p1.age = " + p1.age);

        System.out.println("Player.NumberOfLegs = " + Player.NumberOfLegs);

        Player.NumberOfLegs = 5 ;
        System.out.println("Player.NumberOfLegs = " + Player.NumberOfLegs);



        Player[] players = new Player[3];
        System.out.println("players = " + players);
        for(int i=0;i<players.length;i++) {
            players[i] = new Player();
            System.out.printf(Locale.getDefault(), "players[%d] = %s%n", i, players[i]);
        }


        new Player();
//        Desh desh = new Desh();
    }
}

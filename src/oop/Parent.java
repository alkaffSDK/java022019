package oop;

public class Parent {

    private  int A ;
    int B ;
    protected int C ;
    public int D ;

    @Override
    public String toString() {
        return "Parent{" +
                "A=" + A +
                ", B=" + B +
                ", C=" + C +
                ", D=" + D +
                '}';
    }
}

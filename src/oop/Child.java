package oop;

public class Child extends Parent {

    private  int A ;
    int B ;
    protected int C ;
    public int D ;

    public void method()
    {
       // M = 10 ;
        //A = 10 ;
//        B = 10 ;
//        C = 10 ;
//        D = 10 ;
        super.B = 50 ;
        super.C = 60 ;
        super.D = 70 ;

        //S = 10 ;
    }

    @Override
    public String toString() {
        return "Child{" +
//                "A=" + A +
                ", B=" + B +
                ", C=" + C +
                ", D=" + D +
                '}'+ super.toString();
    }
}

package oop;

public class InheritanceDemo {
    public static void main(String[] args) {

        Child c = new Child();
        System.out.println(c);

        c.method();
        c.B = 10;
        c.C = 20 ;
        c.D = 30 ;
        System.out.println(c);

    }
}

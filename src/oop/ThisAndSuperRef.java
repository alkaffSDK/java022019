package oop;

public class ThisAndSuperRef {

    // In constructors : this and super have another role .
    // this reference : is a reference from inside the object referee to  the object itself

    static  class Parent {
        int d ;
        int c ;
        public  void nonStaticMethod(int d)
        {
            d = 10 ;        // will change the value of the local variable d

            this.d = 8 ;    // will change the value of the instance variable d

            // if there is no local variable that have the same name as an instance variable then you can call it directly

            c = 95 ;
        }

        public static   void staticMethod()
        {
            //this.c ;        // you can't use this or super from static methods
        }

        @Override
        public String toString() {
            return "Parent{" +
                    "d=" + d +
                    ", c=" + c +
                    '}';
        }
    }

    // super reference : is a reference from inside the object  referee to  the parent object.
    static  class  Child extends  Parent {

        int d ;
        public  void nonStaticMethod(int d)
        {
            d = 10 ;        // will change the value of the local variable d

            this.d = 5 ;    // will change the value of the instance variable d inside the child object
            super.d = 20 ;  // will change the value of the instance variable d inside the Parent object

            // if there is no local variable that have the same name as an instance variable then you can call it directly

            c = 10 ;
        }

        public static   void staticMethod()
        {
            //this.c ;        // you can't use this or super from static methods
            // super.d ;
        }

        @Override
        public String toString() {
            return "Child{" +
                    "d=" + d +
                    '}'+ super.toString() ; // THIS WILL CALL THE TOSTRING FORM THE PARENT CLASS
        }
    }



}

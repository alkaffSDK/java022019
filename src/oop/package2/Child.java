package oop.package2;

import oop.package1.Encapsulation;

public class Child extends Encapsulation {

    void methodInChild() {
//        Private = 10 ;
//        DefaultOrpackage = 10 ;
        Protected = 10;
        super.Protected = 20;
        Public = 10;

        Encapsulation obj = new Encapsulation();
        // obj.Private = 10 ;
//        obj.DefaultOrpackage = 10 ;
//        obj.Protected = 10 ;
        obj.Public = 10;
    }

    public static void main(String[] args) {
        Encapsulation obj = new Encapsulation();
        // obj.Private = 10 ;
//        obj.DefaultOrpackage = 10 ;

//       obj.Protected = 10 ;
        obj.Public = 10;

    }
}

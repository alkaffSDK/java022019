import java.util.*;

public class CollectionDemo {
    public static void main(String[] args) {

        System.out.println("=========ArrayList===========");
        ArrayList<Integer> arrayList = new ArrayList<>();

        System.out.println("arrayList = " + arrayList);
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(4);
        arrayList.add(6);
        arrayList.add(2);
        arrayList.add(10);
        System.out.println("arrayList = " + arrayList);
        arrayList.clear();
        System.out.println("arrayList = " + arrayList);

        arrayList.ensureCapacity(50);


        System.out.println("=========Vector===========");
        Vector<Integer> vector = new Vector<>();

        System.out.println("=========HashSet===========");
        HashSet<Integer> hashSet = new HashSet<>();
        System.out.println("hashSet = " + hashSet);
        hashSet.add(10);
        hashSet.add(1);
        hashSet.add(2);
        hashSet.add(4);
        hashSet.add(6);
        hashSet.add(2);

        System.out.println("hashSet = " + hashSet);

        HashSet<String> hashSet2 = new HashSet<>();
        System.out.println("hashSet2 = " + hashSet2);
        hashSet2.add("First");
        hashSet2.add("Second");
        hashSet2.add("Third");
        hashSet2.add("fourth");
        hashSet2.add("Fourth");

        System.out.println("hashSet2 = " + hashSet2);
        for(String s : hashSet2)
            System.out.print(s+ ":"+Integer.toHexString(s.hashCode())+ ",\t");


        System.out.println();

        System.out.println("=========TreeSet===========");


        TreeSet<String> treeSet = new TreeSet<>();

        treeSet.add("First");
        treeSet.add("Second");
        treeSet.add("Third");
        treeSet.add("fourth");
        treeSet.add("Fourth");

        System.out.println("treeSet = " + treeSet);

        System.out.println("=========Queue===========");

        Queue<String> queue = new LinkedList<>();
        queue.add("First");
        queue.add("Second");
        queue.add("Third");
        queue.add("fourth");
        queue.add("Fourth");

        System.out.println("queue = " + queue);


        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());


        System.out.println("queue = " + queue);
        System.out.println("=========PriorityQueue===========");

        PriorityQueue<String> priorityQueue = new PriorityQueue<>();
        priorityQueue.add("First");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Second");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Third");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("fourth");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Fourth");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Fifth");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Tenth");

        System.out.println("priorityQueue = " + priorityQueue);

        System.out.println(priorityQueue.poll());
        System.out.println("priorityQueue = " + priorityQueue);

        System.out.println(priorityQueue.poll());
        System.out.println("priorityQueue = " + priorityQueue);

        System.out.println(priorityQueue.poll());
        System.out.println("priorityQueue = " + priorityQueue);

        System.out.println(priorityQueue.poll());
        System.out.println("priorityQueue = " + priorityQueue);

        System.out.println(priorityQueue.poll());
        System.out.println("priorityQueue = " + priorityQueue);

        System.out.println(priorityQueue.poll());
        System.out.println("priorityQueue = " + priorityQueue);

        PriorityQueue<Integer> integerPriorityQueue = new PriorityQueue<>();
        integerPriorityQueue.add(2);
        integerPriorityQueue.add(5);
        integerPriorityQueue.add(-8);
        integerPriorityQueue.add(1);
        integerPriorityQueue.add(7);
        integerPriorityQueue.add(9);
        integerPriorityQueue.add(0);
        System.out.println("integerPriorityQueue = " + integerPriorityQueue);
        while(!integerPriorityQueue.isEmpty())
            System.out.print(integerPriorityQueue.poll()+ ", ");

        System.out.println();


        HashMap<String,Object> map = new HashMap<>();
        map.put("Name","Ahmed Alkaff");
        map.put("Email","a.Alkaff@sdkjordan.com");
        map.put("Phone","07889462541");
        map.put("Email2","a.Alkaff@sdkjordan.org");
        System.out.println("map = " + map);

        LinkedList<Map<String,Object>> contacts = new LinkedList<>();

        contacts.add(map);


        HashMap<String,Object> map1 = new HashMap<>();
        map1.put("Name","Mosab Malkawi");
        map1.put("Email","m.malkawi@sdkjordan.com");
        map1.put("Phone","0789888400");

        contacts.add(map1);

        System.out.println("contacts = " + contacts);

        for(int i=0; i< contacts.size();i++)
        {
            System.out.println("contacts.get(i).keySet() = " + contacts.get(i).keySet());
            System.out.println("contacts.get(i).values() = " + contacts.get(i).values());

            if(contacts.get(i).containsKey("Email2"))
                System.out.println(contacts.get(i).get("Email2"));
        }

        for( Map<String,Object> m : contacts)
        {
            System.out.println("m.keySet() = " + m.keySet());
            System.out.println("m.values() = " + m.values());
            if(m.containsKey("Email2")) {
                System.out.println(m.get("Email2"));
                m.put("Email2","new emails") ;
            }
        }

        System.out.println("contacts = " + contacts);

    }
}

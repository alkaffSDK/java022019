package Tasks;

import jdk.jshell.spi.ExecutionControl;

import java.io.*;
import java.util.*;

public class Task2 {
    public static final String FILE_PATH = "DataSet.txt";
    public static void main(String[] args) {

        ArrayList<Record> records = new ArrayList<>();
        String line = "";
        String[] splited ;
        Record record ;

        try (BufferedReader reader = new BufferedReader(new FileReader(new File(FILE_PATH))))
        {
            reader.readLine() ;
            while ((line = reader.readLine()) != null)
            {
                splited = line.split(",") ;
                record = new Record(splited[0],splited[1],splited[2],splited[3],splited[4]);
                records.add(record) ;

            }
            int femaleCounter = 0, malesCounter = 0 , allCounter = 0 ;
            for (Record r : records) {
                allCounter++;

                if (r.getGenderBoolean())
                    femaleCounter++;
                else if (r.getGenderBoolean() == false)
                    malesCounter++;
            }
            // OR
            System.out.printf(Locale.getDefault(),"Females : %d , Males : %d , ALL : %d%n",femaleCounter,malesCounter,allCounter);

            femaleCounter = count(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    if(record.getGenderBoolean() )
                        return true;
                    return  false ;
                }
            });

           Condition<Record> males = new Condition<Record>() {
               @Override
               public boolean test(Record record) {
                   if(! record.getGenderBoolean() )
                       return true;
                   return  false ;
               }
           };
            malesCounter = count(records,males);

            allCounter = count(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return true;
                }
            });
            System.out.printf(Locale.getDefault(),"Females : %d , Males : %d , ALL : %d%n",femaleCounter,malesCounter,allCounter);


            System.out.println("The total records :" + count(records));


            System.out.println("Education 12 :"+ count(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return  record.getEducation() == 12;
                }
            }));

            System.out.println("Females in Education 12 :"+ count(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return  record.getGenderBoolean() == true &&  record.getEducation() == 12;
                }
            }));
            System.out.println("Females in Education 12 with score more than 5:"+ count(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return  record.getGenderBoolean() == true &&  record.getEducation() == 12 && record.getScore() > 5;
                }
            }));

            System.out.println("Males in Education 0 with score of 10:"+ count(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return  record.getGenderBoolean() == false &&  record.getEducation() == 0 && record.getScore() == 10;
                }
            }));


            report(records, new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return false;
                }
            });
         //   print(records);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int count(ArrayList<Record> list)
    {
        int counter = 0 ;
        for (Record r : list)
            counter++ ;

        return  counter ;
    }

    public static int count(ArrayList<Record> list,Condition<Record> condition)
    {
        int counter = 0 ;
        for (Record r : list)
            if(condition.test(r))
                counter++ ;

        return  counter ;
    }

    public static void print(ArrayList<Record> list)
    {
        for (Record r : list)
            System.out.println(r);

    }


    public static void print(List<Record> list, Condition<Record> condition) {
        // TODO: Implement this method

    }

    public static void print(List<Record> list, Condition<Record> condition, String outputFile) {
        // TODO: Implement this method

        try (PrintWriter writer = new PrintWriter(new FileWriter(new File(outputFile))))
        {
            // To write use writer.println();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public static void report(List<Record> list, Condition<Record> condition) {
        // TODO: Implement this method
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(new File("Report.txt"),true)))
        {
            // To write use writer.println();
            writer.println("This will be written to the report file");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}

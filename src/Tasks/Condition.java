package Tasks;

public interface Condition<T> {
    boolean test(T t);
}

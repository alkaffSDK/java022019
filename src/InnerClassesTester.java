public class InnerClassesTester {
    public static void main(String[] args) {

        Outer outer = new Outer();
        Outer.NonStaticInner nonStaticInner = outer.new NonStaticInner();
        // or
        Outer.NonStaticInner nonStaticInner1 = new Outer().new NonStaticInner();

        Outer.StaticInner staticInner = new Outer.StaticInner();
    }
}
